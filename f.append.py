# file=open('myfile.txt','w')                       #override text
file=open('myfile.txt','a')                        #append text
print(file.tell())                                  #tells position of pointer
# file=open('myfile.txt','x')                   #create file but give error if file aslready exists
file.write('Arise! Awake! \n and stop')
print(file.tell())
file.write('some text')
file.close()