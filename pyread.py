f=open('myfile.txt','r')
# data=f.read()
# print(data)   

# #read single line
# d=f.readline()

#read specific number of characters
# d=f.readline(2)
d=f.readline(5)
# for i in d:
#     print(i)

#read next line
d=f.readline()
print(d)

d=f.readline()
print(d)

#read all lines
d=f.readlines()         #stores data in a list
print(d[1])

# fileclose
f.close()